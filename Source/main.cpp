#include "Misc.h"
#include "ModHostClient.h"
#include "PedalBank.h"
#include "PedalboardManager.h"
#include "Settings/GeneralSettings.h"
#include "Settings/PedalboardInfo.h"

#include <chrono>
#include <iostream>

#include "cppgpio.hpp"
#include "cppgpio/MultiplexerPushButton8.hpp"


const PedalboardInfo g_info{ PedalBank{}, ModHostClient{ 5555, 5556 }, GeneralSettings{} };


int main(int, char**)
{
	GPIO::HitachiLCD LCD{ 4, 20, "/dev/i2c-1", 0x3f };
	LCD.backlight(true);


	GPIO::PushButton Button{ 17, GPIO::GPIO_PULL::UP };
	Button.f_switched = [&LCD](bool bState)
	{
		LCD.pos(0, 0);
		LCD.fill_line();

		const std::string ButtonMessage{ "Rotary Button: " + std::to_string(bState) };
		LCD.write(0, (20 - ButtonMessage.length()) / 2, ButtonMessage);
	};

	Button.f_switched(false);
	Button.start();


	GPIO::RotaryDial RotaryButton{ 22, 27, GPIO::GPIO_PULL::UP };
	RotaryButton.f_dialed = [&LCD](bool bUp, long value)
	{
		LCD.pos(1, 0);
		LCD.fill_line();

		const std::string RotaryMessage{ "Rotary Value: " + std::to_string(value) };
		LCD.write(1, (20 - RotaryMessage.length()) / 2, RotaryMessage);
	};

	RotaryButton.f_dialed(true, 0);
	RotaryButton.start();


	GPIO::MultiplexerControl8 MultiplexerControl{ 23, 24, 25 };
	GPIO::MultiplexerPushButton8 PedalFootSwitches{ 16, MultiplexerControl };
	PedalFootSwitches.f_pushed = [&LCD](uint8_t ButtonNumber, const std::chrono::nanoseconds PressTime)
	{
		LCD.pos(2, 0);
		LCD.fill_line();

		const std::string ButtonMessage{ "Press Button: " + std::to_string(ButtonNumber) };
		LCD.write(2, (20 - ButtonMessage.length()) / 2, ButtonMessage);
		std::cout << ButtonMessage << std::endl;
	};
	PedalFootSwitches.f_held = [&LCD](uint8_t ButtonNumber, const std::chrono::nanoseconds PressTime)
	{
		LCD.pos(3, 0);
		LCD.fill_line();

		const std::string HeldMessage{ "Held Button: " + std::to_string(ButtonNumber) };
		LCD.write(3, (20 - HeldMessage.length()) / 2, HeldMessage);
		std::cout << HeldMessage << std::endl;
	};

	MultiplexerControl.start();
	PedalFootSwitches.start();


	GPIO::MultiplexerOutput8 ActiveLedsManager{ 5, MultiplexerControl };
	ActiveLedsManager.SetOutputBoolFlag(true, 2);
	ActiveLedsManager.SetOutputBoolFlag(true, 4);
	ActiveLedsManager.start();


	std::this_thread::sleep_for(std::chrono::seconds(60));


	std::cout << "Multi-pedalboard Start!\n";

	for (const std::pair<const std::string, std::vector<Pedal>>& mPedalCategory : g_info.PedalBankRef.GetPedals())
	{
		for (const Pedal& mPedal : mPedalCategory.second)
		{
			mPedal.PrintData();
			std::cout << "------------------------------\n";
		}
	}

	std::cout << std::endl;

	//	if (!g_info.ModHostClientRef.HaveValidClientConnection())
	//	{
	//		std::cerr << "There is no valid Mod-Host Server available" << std::endl;
	//		return 0;
	//	}
	//
	//
	//	float OutValue{ 0.f };
	//	std::string CpuCommand{ "cpu_load" };
	//	g_info.ModHostClientRef.SendCommand(CpuCommand, OutValue);
	//	std::cout << "CPU USAGE: " << OutValue << std::endl << std::endl;
	//
	//
	//	PedalboardManager mPedalboardManager;
	//	mPedalboardManager.Initialize();
	//	std::cout << std::endl;
	////	sleep(1);
	//
	//	mPedalboardManager.AddNewPedalboard();
	////	sleep(1);
	//
	//	mPedalboardManager.MoveToPedalboard(ESwapPedalboard::Next);
	////	sleep(1);
	//
	//	mPedalboardManager.MoveToPedalboard(ESwapPedalboard::Next);
	////	sleep(1);
	//
	//	mPedalboardManager.MoveToPedalboard(ESwapPedalboard::Next);
	////	sleep(1);
	//
	//	mPedalboardManager.SwapCurrentPedalboardWith(ESwapPedalboard::Next);
	////	sleep(1);
	//
	//	mPedalboardManager.RemoveCurrentPedalboard();
	////	sleep(1);

	return 0;
}
