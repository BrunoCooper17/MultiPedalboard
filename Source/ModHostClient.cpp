#include "ModHostClient.h"

#include <iostream>
#include <map>
#include <utility>

/** Sockets */
#include <arpa/inet.h>
#include <sys/socket.h>
#include <unistd.h>


ModHostClient::ModHostClient(int32_t InWritePort, int32_t InFeedbackPort)
	: WriteSocket{ CreateSocket() }
	, FallbackSocket{ CreateSocket() }
	, bIsValidClient{ false }
{
	bIsValidClient |= ConnectWithPort(WriteSocket, InWritePort);
	bIsValidClient |= ConnectWithPort(FallbackSocket, InFeedbackPort);
}


ModHostClient::ModHostClient(const ModHostClient& Other)
	: WriteSocket{ Other.WriteSocket }
	, FallbackSocket{ Other.FallbackSocket }
	, bIsValidClient{ Other.bIsValidClient }
{}


ModHostClient::ModHostClient(ModHostClient&& Other) noexcept
	: WriteSocket{ std::exchange(Other.WriteSocket, -1) }
	, FallbackSocket{ std::exchange(Other.FallbackSocket, -1) }
	, bIsValidClient{ std::exchange(Other.bIsValidClient, false) }
{}


ModHostClient::~ModHostClient()
{
	close(WriteSocket);
	close(FallbackSocket);

	WriteSocket = FallbackSocket = -1;
}


int32_t ModHostClient::SendCommand(const std::string& InCommand, float& OutValue) const
{
	if (!bIsValidClient)
	{
		std::cerr << "SendCommand: Trying to send command with invalid client" << std::endl;
		return -902;
	}

	const int32_t BufferSize{ 1024 };
	char Buffer[BufferSize]{ 0 };
	const int32_t Socket{ GetValidSocket() };

	send(Socket, InCommand.c_str(), InCommand.length(), 0);
	read(Socket, Buffer, BufferSize);

	std::string BufferStr{ Buffer };

	BufferStr = BufferStr.erase(0, 5);
	const auto SplitPos{ BufferStr.find(' ') };
	if (SplitPos != std::string::npos)
	{
		OutValue = std::stof(BufferStr.substr(SplitPos + 1));
		return std::stoi(BufferStr.substr(0, SplitPos));
	}

	return std::stoi(BufferStr);
}


const std::string& ModHostClient::GetErrorMessage(int32_t InErrorCode) const
{
	const static std::map<const int32_t, const std::string> ErrorTable{ { -1, "ERR_INSTANCE_INVALID" },
		{ -2, "ERR_INSTANCE_ALREADY_EXISTS" }, { -3, "ERR_INSTANCE_NON_EXISTS" }, { -4, "ERR_INSTANCE_UNLICENSED" },
		{ -101, "ERR_LV2_INVALID_URI" }, { -102, "ERR_LV2_INSTANTIATION" }, { -103, "ERR_LV2_INVALID_PARAM_SYMBOL" },
		{ -104, "ERR_LV2_INVALID_PRESET_URI" }, { -105, "ERR_LV2_CANT_LOAD_STATE" },
		{ -201, "ERR_JACK_CLIENT_CREATION" }, { -202, "ERR_JACK_CLIENT_ACTIVATION" },
		{ -203, "ERR_JACK_CLIENT_DEACTIVATION" }, { -204, "ERR_JACK_PORT_REGISTER" },
		{ -205, "ERR_JACK_PORT_CONNECTION" }, { -206, "ERR_JACK_PORT_DISCONNECTION" },
		{ -301, "ERR_ASSIGNMENT_ALREADY_EXISTS" }, { -302, "ERR_ASSIGNMENT_INVALID_OP" },
		{ -303, "ERR_ASSIGNMENT_LIST_FULL" }, { -304, "ERR_ASSIGNMENT_FAILED" },
		{ -401, "ERR_CONTROL_CHAIN_UNAVAILABLE" }, { -402, "ERR_LINK_UNAVAILABLE" }, { -901, "ERR_MEMORY_ALLOCATION" },
		{ -902, "ERR_INVALID_OPERATION" } };

	const static std::string UNKNOWN_ERROR{ "ERR_UNKNOWN" };

	const auto& Error{ ErrorTable.find(InErrorCode) };
	if (Error != ErrorTable.cend())
	{
		return Error->second;
	}

	return UNKNOWN_ERROR;
}


int32_t ModHostClient::CreateSocket() const
{
	const int32_t Socketfd{ socket(AF_INET, SOCK_STREAM, 0) };
	if (Socketfd < 0)
	{
		perror("While creating socket");
	}

	return Socketfd;
}


bool ModHostClient::ConnectWithPort(int32_t& Socket, const int32_t Port)
{
	struct sockaddr_in serv_addr{};
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_port = htons(Port);

	/** Convert IPv4 and IPv6 addresses from text to binary form */
	if (inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr) <= 0)
	{
		perror("While converting IP Address");
		return false;
	}

	if (connect(Socket, (struct sockaddr*)&serv_addr, sizeof(serv_addr)) < 0)
	{
		perror("While connecting to mod-host server");
		return false;
	}

	return true;
}


inline int32_t ModHostClient::GetValidSocket() const
{
	return WriteSocket >= 0 ? WriteSocket : FallbackSocket >= 0 ? FallbackSocket : -1;
}
