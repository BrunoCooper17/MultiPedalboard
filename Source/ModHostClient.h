#pragma once

#include <cstdint>
#include <string>


class ModHostClient
{
public:
	ModHostClient() = delete;
	explicit ModHostClient(int32_t InWritePort, int32_t InFeedbackPort);
	ModHostClient(const ModHostClient& Other);
	ModHostClient(ModHostClient&& Other) noexcept;
	~ModHostClient();


	int32_t SendCommand(const std::string& InCommand, float& OutValue) const;
	[[nodiscard]] const std::string& GetErrorMessage(int32_t InErrorCode) const;


	[[nodiscard]] bool HaveValidClientConnection() const { return bIsValidClient; }


private:
	[[nodiscard]] int32_t CreateSocket() const;
	bool ConnectWithPort(int32_t& Socket, int32_t Port);


	[[nodiscard]] int32_t GetValidSocket() const;


private:
	int32_t WriteSocket;
	int32_t FallbackSocket;
	bool bIsValidClient;
};
