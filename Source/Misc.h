//
// Created by brunocooper on 5/6/23.
//

#pragma once

#include <string>


class Misc
{
public:
	/**
	 * Execute shell cmd and returns the output as string.
	 * Example:
	 *   - std::string CpuCommand{ R"(echo "CPU Usage: "$[100-$(vmstat 1 2|tail -1|awk '{print $15}')]"%")" };
	 */
	static std::string command_exec(const char* cmd);
};
