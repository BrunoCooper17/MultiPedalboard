#pragma once

// Lilv
#include <map>
#include <string>
#include "lilv-0/lilv/lilv.h"


enum class ETypePort : int8_t
{
	AUDIO_IN = 0,
	AUDIO_OUT,
	CONTROL_IN,

	INVALID = -1 // DESTROY IMMEDIATELY :)
};


class Port
{
public:
	explicit Port(std::string InSymbol, ETypePort InType);
	explicit Port(const LilvPlugin* Plugin, const LilvPort* Port, ETypePort InType);
	explicit Port(const LilvPlugin* Plugin, const LilvPort* Port, ETypePort InType, float InMinValue, float InMaxValue,
		float InDefaultValue);


	[[nodiscard]] static ETypePort GetPortType(const LilvPlugin* Plugin, const LilvPort* Port);


	[[nodiscard]] ETypePort GetType() const { return Type; }


	[[nodiscard]] const std::string& GetName() const { return Name; }
	[[nodiscard]] const std::string& GetSymbolStr() const { return Symbol; }


	[[nodiscard]] bool IsScalarValue() const { return bIsScalar; }


	[[nodiscard]] float GetMinValue() const { return Min; }
	[[nodiscard]] float GetMaxValue() const { return Max; }
	[[nodiscard]] float GetDefaultValue() const { return Default; }
	[[nodiscard]] float GetCurrentValue() const { return Current; }


	void AddDeltaToCurrentValue(float Delta);


	[[nodiscard]] std::string GetCurrentScalarName() const;


	void PrintData() const;


private:
	ETypePort Type = ETypePort::INVALID;


	std::string Name;
	std::string Symbol;


	bool bIsScalar = false;
	std::map<int32_t, std::string> ScalarValues;


	float Min = 0.f;
	float Max = 0.f;
	float Default = 0.f;
	float Current = 0.f;
};
