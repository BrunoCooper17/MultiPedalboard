#include "Port.h"
#include "PedalBank.h"

#include <algorithm>
#include <cmath>
#include <cstdint>
#include <iostream>
#include <string>
#include <utility>


Port::Port(std::string InSymbol, const ETypePort InType)
	: Symbol{ std::move(InSymbol) }
	, Type{ InType }
{
}


Port::Port(const LilvPlugin* PluginPtr, const LilvPort* PortPtr, ETypePort InType)
	: Type{ InType }
{
	// -------------- SYMBOL & NAME --------------
	const LilvNode* symbol{ lilv_port_get_symbol(PluginPtr, PortPtr) };
	Symbol = lilv_node_as_string(symbol);

	LilvNode* name{ lilv_port_get_name(PluginPtr, PortPtr) };
	Name = lilv_node_as_string(name);
	lilv_node_free(name);
	// -------------------------------------------

	// -------------- SCALAR VALUES ---------------
	LilvScalePoints* ScalePoints{ lilv_port_get_scale_points(PluginPtr, PortPtr) };
	LILV_FOREACH(scale_points, ScaleIndex, ScalePoints)
	{
		const LilvScalePoint* ScalePoint{ lilv_scale_points_get(ScalePoints, ScaleIndex) };
		ScalarValues.insert_or_assign(lilv_node_as_int(lilv_scale_point_get_value(ScalePoint)),
			lilv_node_as_string(lilv_scale_point_get_label(ScalePoint)));
		bIsScalar = true;
	}

	lilv_scale_points_free(ScalePoints);
	// --------------------------------------------
}


Port::Port(const LilvPlugin* PluginPtr, const LilvPort* PortPtr, ETypePort InType, float InMinValue, float InMaxValue,
	float InDefaultValue)
	: Port{ PluginPtr, PortPtr, InType }
{
	if (InType != ETypePort::CONTROL_IN)
	{
		return;
	}

	Min = !std::isnan(InMinValue) ? InMinValue : 0.f;
	Max = !std::isnan(InMaxValue) ? InMaxValue : 0.f;
	Default = !std::isnan(InDefaultValue) ? InDefaultValue : 0.f;
}


ETypePort Port::GetPortType(const LilvPlugin* Plugin, const LilvPort* Port)
{
	// ---------------- PORT TYPE ----------------
	// AUDIO PORT?
	if (lilv_port_is_a(Plugin, Port, PedalBank::AudioClass))
	{
		// INPUT
		if (lilv_port_is_a(Plugin, Port, PedalBank::InputClass))
		{
			return ETypePort::AUDIO_IN;
		}

		// OUTPUT
		else if (lilv_port_is_a(Plugin, Port, PedalBank::OutputClass))
		{
			return ETypePort::AUDIO_OUT;
		}

		return ETypePort::INVALID;
	}
	// CONTROL PORT?
	else if (lilv_port_is_a(Plugin, Port, PedalBank::ControlClass) &&
		lilv_port_is_a(Plugin, Port, PedalBank::InputClass))
	{
		return ETypePort::CONTROL_IN;
	}

	return ETypePort::INVALID;
	// -------------------------------------------
}


void Port::AddDeltaToCurrentValue(float Delta)
{
	if (bIsScalar && std::abs(Delta) > 0)
	{
		Delta = Delta < 0.f ? -1.f : 1.f;
	}

	Current += Delta;
	Current = std::clamp(Current, GetMinValue(), GetMaxValue());
}


std::string Port::GetCurrentScalarName() const
{
	return bIsScalar ? ScalarValues.at(static_cast<int32_t>(Current)) : "";
}


void Port::PrintData() const
{
	std::cout << "\t" << Name << "\tm:" << Min << "\tM:" << Max << "\td:" << Default << "\tC:" << Current << "\n";
	for (const auto& [ScalarIndex, ScalarName] : ScalarValues)
	{
		std::cout << "\t\t" << ScalarIndex << " => " << ScalarName << "\n";
	}
}
