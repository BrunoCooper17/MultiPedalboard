#include "Pedalboard.h"

#include "ModHostCommands/Commands.h"
#include "Pedal.h"
#include "Settings/PedalboardInfo.h"

#include "toml.hpp"

#include <algorithm>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <utility>


Pedalboard::Pedalboard(const uint32_t InIndex)
	: bIsActive{ false }
	, Index{ InIndex }
	, Name{ g_info.GlobalSettings.DefaultName }
	, NumPedals{ g_info.GlobalSettings.MaxPedals + (g_info.bSFXLoop ? g_info.GlobalSettings.MaxPedalsSFX : 0) }
{
	/** Fill with dummy (invalid) pedals */
	for (uint32_t count{ 0 }; count < NumPedals; ++count)
	{
		Pedals.emplace_back();
	}

	LoadPedalboardConfiguration();
}


void Pedalboard::Activate()
{
	bIsActive = true;

	/** Front pedals */
	PlugPedal(-1, EPlugOperation::Connect, EPedalLocation::Front);
	PlugPedal(g_info.GlobalSettings.MaxPedals, EPlugOperation::Connect, EPedalLocation::Front);

	if (g_info.bSFXLoop)
	{
		PlugPedal(g_info.GlobalSettings.MaxPedals - 1, EPlugOperation::Connect, EPedalLocation::SFXLoop);
		PlugPedal(Pedals.size(), EPlugOperation::Connect, EPedalLocation::SFXLoop);
	}
}


void Pedalboard::Deactivate()
{
	bIsActive = false;

	PlugPedal(-1, EPlugOperation::Disconnect, EPedalLocation::Front);
	PlugPedal(g_info.GlobalSettings.MaxPedals, EPlugOperation::Disconnect, EPedalLocation::Front);

	if (g_info.bSFXLoop)
	{
		PlugPedal(g_info.GlobalSettings.MaxPedals - 1, EPlugOperation::Disconnect, EPedalLocation::SFXLoop);
		PlugPedal(Pedals.size(), EPlugOperation::Disconnect, EPedalLocation::SFXLoop);
	}
}


bool Pedalboard::LoadPedals() const
{
	uint32_t ValidPedals{ 0 };
	for (int32_t idx{ 0 }; idx < NumPedals; ++idx)
	{
		const Pedal& PedalRef{ Pedals[idx] };
		if (!PedalRef.IsValid())
		{
			continue;
		}

		++ValidPedals;
		if (LoadPedal(idx) && !PedalRef.IsActive())
		{
			ModHostCommands::BypassPedal(g_info.ModHostClientRef, idx + GetBaseIndex(), true);
		}
	}

	if (!ValidPedals)
	{
		return false;
	}

	for (int32_t idx{ 0 }; idx < NumPedals; ++idx)
	{
		if (Pedals[idx].IsValid())
		{
			/** Connect our new pedal */
			PlugPedal(idx, EPlugOperation::Connect, GetPedalLocation(idx));
		}
	}

	return true;
}


bool Pedalboard::UnloadPedals() const
{
	uint32_t ValidPedals{ 0 };
	for (int32_t idx{ 0 }; idx < NumPedals; ++idx)
	{
		if (!Pedals[idx].IsValid())
		{
			continue;
		}

		++ValidPedals;
		ModHostCommands::RemovePedal(g_info.ModHostClientRef, idx + GetBaseIndex());
	}

	return ValidPedals > 0;
}


void Pedalboard::LoadPedalboardConfiguration()
{
	try
	{
		const std::string ConfigFilename{ GetConfigFilename() };
		if (!std::filesystem::exists(ConfigFilename))
		{
			std::cerr << "Pedalboard configuration [" << Index << "].toml does not exist..." << std::endl;
			return;
		}

		const auto& Config{ toml::parse_file(ConfigFilename) };
		Name = Config["Name"].as_string()->get();

		const auto PedalsConfig{ Config["Pedal"].as_array() };
		if (!PedalsConfig)
		{
			std::cerr << "No pedals in Pedalboard [" << Index << "]" << std::endl;
			return;
		}

		for (auto PedalIter{ PedalsConfig->cbegin() }; PedalIter != PedalsConfig->cend(); ++PedalIter)
		{
			const toml::table PedalConfig{ *PedalIter->as_table() };
			const auto PedalIndex{ PedalConfig["Index"].as_integer()->get() };
			const std::string PedalUri{ PedalConfig["Uri"].as_string()->get() };
			const std::string PedalCategory{ PedalConfig["Category"].as_string()->get() };
			const bool PedalIsActive{ PedalConfig["bIsActive"].as_boolean()->get() };

			const auto PortsConfig{ PedalConfig["Controls"].as_array() };
			if (!PortsConfig)
			{
				std::cerr << "No ports in Pedal [" << PedalUri << "]" << std::endl;
				continue;
			}

			Pedal PedalToLoad{ g_info.PedalBankRef.GetPedal(PedalCategory, PedalUri) };
			std::vector<Port>& ControlPorts{ PedalToLoad.GetControlPorts() };
			for (auto PortIter{ PortsConfig->cbegin() }; PortIter != PortsConfig->cend(); ++PortIter)
			{
				const toml::table PortConfig{ *PortIter->as_table() };
				const std::string PortSymbol{ PortConfig["Symbol"].as_string()->get() };
				const double PortValue{ PortConfig["Value"].as_floating_point()->get() };

				const auto IsPortSymbol{ [&PortSymbol](const Port& InPort)
					{
						return InPort.GetSymbolStr() == PortSymbol;
					} };
				auto PortFound{ std::find_if(ControlPorts.begin(), ControlPorts.end(), IsPortSymbol) };
				if (PortFound != ControlPorts.end())
				{
					PortFound->AddDeltaToCurrentValue(static_cast<float>(PortValue) - PortFound->GetDefaultValue());
				}
			}

			PedalToLoad.SetActive(PedalIsActive);
			AddPedalAt(std::move(PedalToLoad), PedalIndex, false);
		}
	}
	catch (const toml::parse_error& Exception)
	{
		std::cerr << "Error parsing file [" << Exception.source().path << "]: " << Exception.description() << " ("
				  << Exception.source().begin << ")" << std::endl;
	}
}


void Pedalboard::SavePedalboardConfiguration() const
{
	toml::table PedalboardToml{ { "Name", Name } };

	toml::array PedalsToml;
	for (int32_t PedalIndex{ 0 }; PedalIndex < Pedals.size(); ++PedalIndex)
	{
		Pedal PedalRef{ Pedals[PedalIndex] };
		if (!PedalRef.IsValid())
		{
			continue;
		}

		toml::table PedalToml{ { "Index", PedalIndex }, { "Uri", PedalRef.GetURI() },
			{ "Category", PedalRef.GetCategory() }, { "bIsActive", PedalRef.IsActive() } };

		toml::array PortsToml;
		for (const Port& Port : PedalRef.GetControlPorts())
		{
			PortsToml.emplace_back(
				toml::table{ { "Symbol", Port.GetSymbolStr() }, { "Value", Port.GetCurrentValue() } });
		}

		PedalToml.emplace("Controls", std::move(PortsToml));
		PedalsToml.emplace_back(std::move(PedalToml));
	}

	PedalboardToml.emplace("Pedal", std::move(PedalsToml));

	// serializing as TOML
	std::ofstream OutputFile{ GetConfigFilename(), std::ios_base::out | std::ios_base::trunc };
	OutputFile << PedalboardToml << std::endl;
	OutputFile.close();
}


void Pedalboard::SetNewIndex(uint32_t NewIndex, bool bRenameOrSave)
{
	const std::string OldSaveName{ GetConfigFilename() };

	const uint32_t OldIndex = Index;
	Index = NewIndex;

	const std::string NewSaveName{ GetConfigFilename() };

	if (std::filesystem::exists(NewSaveName))
	{
		std::cerr << "WARNING: Attempt to override existing pedalboard save detected. Doing nothing." << std::endl;
		std::cerr << "\tOldSave[" << OldSaveName << "]" << std::endl;
		std::cerr << "\tNewSave[" << NewSaveName << "]" << std::endl;

		Index = OldIndex;
		return;
	}

	if (!bRenameOrSave)
	{
		return;
	}

	if (std::filesystem::exists(OldSaveName))
	{
		std::filesystem::rename(OldSaveName, NewSaveName);
	}
	else
	{
		SavePedalboardConfiguration();
	}
}


Pedal& Pedalboard::GetPedalRefAt(const uint32_t IndexSlot)
{
	return Pedals[IndexSlot];
}


bool Pedalboard::AddPedalAt(Pedal&& InPedal, const uint32_t IndexSlot, const bool bLoad)
{
	if (IndexSlot >= NumPedals || !InPedal.IsValid())
	{
		std::cerr << "AddPedalAt [" << IndexSlot << "]: Invalid parameters" << std::endl;
		return false;
	}

	/** Disconnect anything that is between current IndexSlot */
	PlugPedal(IndexSlot, EPlugOperation::Disconnect, GetPedalLocation(IndexSlot));

	/** Remove any previous instance (if there was any) */
	if (Pedals[IndexSlot].IsValid())
	{
		ModHostCommands::RemovePedal(g_info.ModHostClientRef, IndexSlot + GetBaseIndex());
	}

	/** Set the new pedal in place */
	Pedals[IndexSlot] = std::move(InPedal);

	if (!bLoad)
	{
		return true;
	}

	if (LoadPedal(IndexSlot))
	{
		/** Connect our new pedal */
		PlugPedal(IndexSlot, EPlugOperation::Connect, GetPedalLocation(IndexSlot));
		return true;
	}

	return false;
}


void Pedalboard::RemovePedalAt(const uint32_t IndexSlot)
{
	if (IndexSlot >= NumPedals || !Pedals[IndexSlot].IsValid())
	{
		std::cerr << "RemovePedalAt: Invalid Index [" << IndexSlot << "] (or invalid pedal at index slot)" << std::endl;
		return;
	}

	if (!ModHostCommands::RemovePedal(g_info.ModHostClientRef, IndexSlot + GetBaseIndex()))
	{
		return;
	}

	Pedals[IndexSlot] = std::move(Pedal{});

	PlugPedal(IndexSlot, EPlugOperation::Connect, GetPedalLocation(IndexSlot));
}


void Pedalboard::SwapPedalsAt(const uint32_t IndexSlotA, const uint32_t IndexSlotB)
{
	if (IndexSlotA >= NumPedals || IndexSlotB >= NumPedals)
	{
		std::cerr << "SwapPedalsAt: Invalid parameters [" << IndexSlotA << "," << IndexSlotB << "]" << std::endl;
		return;
	}

	const uint32_t IndexAOffset{ IndexSlotA + GetBaseIndex() };
	const uint32_t IndexBOffset{ IndexSlotB + GetBaseIndex() };

	ModHostCommands::RemovePedal(g_info.ModHostClientRef, IndexAOffset);
	ModHostCommands::RemovePedal(g_info.ModHostClientRef, IndexBOffset);

	auto CacheIterA{ Pedals.begin() + IndexSlotA };
	auto CacheIterB{ Pedals.begin() + IndexSlotB };
	std::iter_swap(CacheIterA, CacheIterB);

	ModHostCommands::AddPedal(g_info.ModHostClientRef, *CacheIterA, IndexAOffset);
	ModHostCommands::AddPedal(g_info.ModHostClientRef, *CacheIterB, IndexBOffset);
	PlugPedal(IndexSlotA, EPlugOperation::Connect, GetPedalLocation(IndexSlotA));
	PlugPedal(IndexSlotB, EPlugOperation::Connect, GetPedalLocation(IndexSlotB));
}


bool Pedalboard::TogglePedalAt(const uint32_t IndexSlot)
{
	if (IndexSlot >= NumPedals)
	{
		std::cerr << "TogglePedalAt: Invalid IndexSlot [" << IndexSlot << "]" << std::endl;
		return false;
	}

	Pedal& PedalRef{ Pedals[IndexSlot] };
	if (!PedalRef.IsValid())
	{
		std::cerr << "TogglePedalAt: Attempt to toggle invalid pedal [" << IndexSlot << "]" << std::endl;
		return false;
	}

	const bool bNewActiveState{ !PedalRef.IsActive() };
	const bool bSuccess{ ModHostCommands::BypassPedal(
		g_info.ModHostClientRef, IndexSlot + GetBaseIndex(), bNewActiveState) };
	if (bSuccess)
	{
		PedalRef.SetActive(bNewActiveState);
	}

	return bSuccess;
}


bool Pedalboard::LoadPedal(uint32_t IndexSlot) const
{
	Pedal PedalRef{ Pedals[IndexSlot] };
	const uint32_t IndexOffset{ IndexSlot + GetBaseIndex() };
	const bool bSuccess{ ModHostCommands::AddPedal(g_info.ModHostClientRef, PedalRef, IndexOffset) };
	if (!bSuccess)
	{
		return false;
	}

	for (const Port& ControlPort : PedalRef.GetControlPorts())
	{
		ModHostCommands::SetPedalParam(
			g_info.ModHostClientRef, IndexOffset, ControlPort.GetSymbolStr(), ControlPort.GetCurrentValue());
	}

	std::cout << "PEDAL LOADED" << std::endl;

	return true;
}


std::string Pedalboard::GetConfigFilename() const
{
	const std::string SettingsFile{ "/Multipedalboard/Pedalboard_" + std::to_string(Index) + ".toml" };
	const std::string Path{ std::getenv("XDG_CONFIG_HOME") };
	return Path.empty() ? std::getenv("HOME") + std::string("/.config") + SettingsFile : Path + SettingsFile;
}


void Pedalboard::PlugPedal(
	const uint32_t IndexSlot, const EPlugOperation PlugOperation, const EPedalLocation PedalLocation) const
{
	const int32_t PrePedalIndex{ FirstValidPedalFrom(static_cast<int32_t>(IndexSlot), ESearchDirection::FromEnd) };
	const int32_t PostPedalIndex{ FirstValidPedalFrom(static_cast<int32_t>(IndexSlot), ESearchDirection::FromStart) };

	if (PedalLocation == GetPedalLocation(IndexSlot) && IndexSlot < NumPedals && Pedals[IndexSlot].IsValid())
	{
		PlugPedals(PrePedalIndex, static_cast<int32_t>(IndexSlot), PlugOperation, PedalLocation);
		PlugPedals(static_cast<int32_t>(IndexSlot), PostPedalIndex, PlugOperation, PedalLocation);
	}
	else
	{
		PlugPedals(PrePedalIndex, PostPedalIndex, PlugOperation, PedalLocation);
	}
}


void Pedalboard::PlugPedals(const int32_t IndexSlotA, const int32_t IndexSlotB, const EPlugOperation PlugOperation,
	const EPedalLocation PedalLocation) const
{
	const int32_t CompareSlotA{
		PedalLocation == EPedalLocation::Front ? -1 : static_cast<int32_t>(g_info.GlobalSettings.MaxPedals - 1)
	};
	const int32_t CompareSlotB{ PedalLocation == EPedalLocation::Front
			? static_cast<int32_t>(g_info.GlobalSettings.MaxPedals)
			: static_cast<int32_t>(NumPedals) };

	const auto& OutputPorts{ IndexSlotA > CompareSlotA ? Pedals[IndexSlotA].GetAudioOutputPorts()
			: PedalLocation == EPedalLocation::Front   ? g_info.Front_PedalPorts.GetAudioInputPorts()
													   : g_info.SFX_PedalPorts.GetAudioInputPorts() };
	const auto& InputPorts{ IndexSlotB < CompareSlotB ? Pedals[IndexSlotB].GetAudioInputPorts()
			: PedalLocation == EPedalLocation::Front  ? g_info.Front_PedalPorts.GetAudioOutputPorts()
													  : g_info.SFX_PedalPorts.GetAudioOutputPorts() };

	const bool bOutputValid{ IndexSlotA > CompareSlotA };
	const bool bInputValid{ IndexSlotB < CompareSlotB };

	const std::string OutputPrefix{ bOutputValid ? "effect_" + std::to_string(IndexSlotA + GetBaseIndex()) + ":"
												 : "system:" };
	const std::string InputPrefix{ bInputValid ? "effect_" + std::to_string(IndexSlotB + GetBaseIndex()) + ":"
											   : "system:" };

	const int32_t NumPortsComparison{ static_cast<int32_t>(OutputPorts.size() - InputPorts.size()) };
	const auto ModPlugMethod{ PlugOperation == EPlugOperation::Connect ? &ModHostCommands::Connect
																	   : &ModHostCommands::Disconnect };

	/** AFAIK, pedals are only mono or stereo */
	switch (NumPortsComparison)
	{
	case -1:
		{
			ModPlugMethod(g_info.ModHostClientRef, OutputPrefix + OutputPorts[0].GetSymbolStr(),
				InputPrefix + InputPorts[0].GetSymbolStr());
			ModPlugMethod(g_info.ModHostClientRef, OutputPrefix + OutputPorts[0].GetSymbolStr(),
				InputPrefix + InputPorts[1].GetSymbolStr());
			break;
		}

	case 0:
		{
			for (uint32_t idx{ 0 }; idx < OutputPorts.size(); ++idx)
			{
				ModPlugMethod(g_info.ModHostClientRef, OutputPrefix + OutputPorts[idx].GetSymbolStr(),
					InputPrefix + InputPorts[idx].GetSymbolStr());
			}
			break;
		}

	case 1:
		{
			ModPlugMethod(g_info.ModHostClientRef, OutputPrefix + OutputPorts[0].GetSymbolStr(),
				InputPrefix + InputPorts[0].GetSymbolStr());
			ModPlugMethod(g_info.ModHostClientRef, OutputPrefix + OutputPorts[1].GetSymbolStr(),
				InputPrefix + InputPorts[0].GetSymbolStr());
			break;
		}

	default:
		std::cerr << "Error found Pedalboard::PlugPedals()" << std::endl;
	}
}


inline EPedalLocation Pedalboard::GetPedalLocation(uint32_t IndexSlot)
{
	return IndexSlot < g_info.GlobalSettings.MaxPedals || IndexSlot == std::numeric_limits<uint32_t>::max()
		? EPedalLocation::Front
		: EPedalLocation::SFXLoop;
}


int32_t Pedalboard::FirstValidPedalFrom(int32_t Reference, const ESearchDirection SearchDirection) const
{
	Reference += static_cast<int32_t>(SearchDirection);

	if (Reference < -1 || Reference >= static_cast<int32_t>(NumPedals + 1))
	{
		std::cerr << "FirstValidPedalFrom [" << Reference << "]: Next reference pedal to check is out of bounds..."
				  << std::endl;

		Reference -= static_cast<int32_t>(SearchDirection);
		return std::clamp(Reference, static_cast<int32_t>(-1), static_cast<int32_t>(NumPedals));
	}

	const int32_t EndIndex{ SearchDirection == ESearchDirection::FromStart ? static_cast<int32_t>(NumPedals)
																		   : static_cast<int32_t>(-1) };

	for (; Reference != EndIndex; SearchDirection == ESearchDirection::FromStart ? ++Reference : --Reference)
	{
		if ((Pedals.cbegin() + Reference)->IsValid())
		{
			break;
		}
	}

	return Reference;
}
