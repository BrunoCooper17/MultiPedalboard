#include "Commands.h"

#include <iostream>


namespace ModHostCommands
{
	bool RemovePedal(const ModHostClient& Client, const uint32_t InstanceNumber)
	{
		float OutValue{ 0.f };
		const std::string Command{ "remove " + std::to_string(InstanceNumber) };
		const int32_t ReturnCode{ Client.SendCommand(Command, OutValue) };

		if (ReturnCode < 0)
		{
			std::cerr << Client.GetErrorMessage(ReturnCode) << ": (" << Command << ")" << std::endl;
			return false;
		}
		return true;
	}
} // namespace ModHostCommands
