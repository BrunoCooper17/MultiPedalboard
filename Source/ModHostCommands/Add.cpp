#include "Commands.h"

#include <iostream>


namespace ModHostCommands
{
	bool AddPedal(const ModHostClient& Client, const Pedal& Pedal, const uint32_t InstanceNumber)
	{
		float OutValue{ 0.f };
		const std::string Command{ "add " + Pedal.GetURI() + " " + std::to_string(InstanceNumber) };
		const int32_t ReturnCode{ Client.SendCommand(Command, OutValue) };

		if (ReturnCode < 0)
		{
			std::cerr << Client.GetErrorMessage(ReturnCode) << ": (" << Command << ")" << std::endl;
			return false;
		}

		return true;
	}
} // namespace ModHostCommands
