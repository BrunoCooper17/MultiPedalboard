#include "Commands.h"

#include <iostream>


namespace ModHostCommands
{
	bool Connect(const ModHostClient& Client, const std::string& OriginPort, const std::string& DestinationPort)
	{
		float OutValue{ 0.f };
		const std::string Command{ "connect " + OriginPort + " " + DestinationPort };
		const int32_t ReturnCode{ Client.SendCommand(Command, OutValue) };

		if (ReturnCode < 0)
		{
			std::cerr << Client.GetErrorMessage(ReturnCode) << ": C(" << Command << ")" << std::endl;
			return false;
		}

		return true;
	}
} // namespace ModHostCommands
