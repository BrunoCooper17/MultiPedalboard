#pragma once

#include "ModHostClient.h"
#include "Pedal.h"


namespace ModHostCommands
{
	bool AddPedal(const ModHostClient& Client, const Pedal& Pedal, uint32_t InstanceNumber);
	bool RemovePedal(const ModHostClient& Client, uint32_t InstanceNumber);
	bool BypassPedal(const ModHostClient& Client, uint32_t InstanceNumber, bool bBypass);

	bool SetPedalParam(
		const ModHostClient& Client, uint32_t InstanceNumber, const std::string& ParamName, float NewValue);
	float GetPedalParam(const ModHostClient& Client, uint32_t InstanceNumber, const std::string& ParamName);

	bool Connect(const ModHostClient& Client, const std::string& OriginPort, const std::string& DestinationPort);
	bool Disconnect(const ModHostClient& Client, const std::string& OriginPort, const std::string& DestinationPort);
} // namespace ModHostCommands
