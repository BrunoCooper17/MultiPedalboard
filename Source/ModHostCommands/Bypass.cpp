#include "Commands.h"

#include <iostream>


namespace ModHostCommands
{
	bool BypassPedal(const ModHostClient& Client, const uint32_t InstanceNumber, const bool bBypass)
	{
		float OutValue{ 0.f };
		const std::string Command{ "bypass " + std::to_string(InstanceNumber) + " " + std::to_string(bBypass) };
		const int32_t ReturnCode{ Client.SendCommand(Command, OutValue) };

		if (ReturnCode < 0)
		{
			std::cerr << Client.GetErrorMessage(ReturnCode) << ": (" << Command << ")" << std::endl;
			return false;
		}

		return true;
	}
} // namespace ModHostCommands
