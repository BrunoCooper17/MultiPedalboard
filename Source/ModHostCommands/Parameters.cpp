#include "Commands.h"

#include <iostream>


namespace ModHostCommands
{
	bool SetPedalParam(
		const ModHostClient& Client, const uint32_t InstanceNumber, const std::string& ParamName, const float NewValue)
	{
		float OutValue{ 0.f };
		const std::string Command{ "param_set " + std::to_string(InstanceNumber) + " " + ParamName + " " +
			std::to_string(NewValue) };
		const int32_t ReturnCode{ Client.SendCommand(Command, OutValue) };

		if (ReturnCode < 0)
		{
			std::cerr << Client.GetErrorMessage(ReturnCode) << ": (" << Command << ")" << std::endl;
			return false;
		}

		return true;
	}


	float GetPedalParam(const ModHostClient& Client, const uint32_t InstanceNumber, const std::string& ParamName)
	{
		float OutValue{ 0.f };
		const std::string Command{ "param_get " + std::to_string(InstanceNumber) + " " + ParamName };
		const int32_t ReturnCode{ Client.SendCommand(Command, OutValue) };

		if (ReturnCode < 0)
		{
			std::cerr << "GetPedalParam(" << Command << "): " << Client.GetErrorMessage(ReturnCode) << std::endl;
			return 0.0f;
		}

		return OutValue;
	}
} // namespace ModHostCommands
