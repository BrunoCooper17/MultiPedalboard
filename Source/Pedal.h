#pragma once

#include "Port.h"
#include "Settings/GeneralSettings.h"

#include <string>
#include <vector>

/** Lilv */
#include "lilv-0/lilv/lilv.h"


class Pedal
{
public:
	Pedal() = default;
	Pedal(const Pedal&) = default;
	Pedal(Pedal&&) = default;

	Pedal(const LilvPlugin* Plugin, std::string InURI, std::string InName, std::string InCategory);
	Pedal(const AudioSettingsData& AudioSettings, bool bSFXLoop);


	Pedal& operator=(const Pedal&) = default;
	Pedal& operator=(Pedal&&) = default;


	void PrintData() const;


	[[nodiscard]] static std::string GetNameFromPlugin(const LilvPlugin* Plugin);

	[[nodiscard]] static std::string GetURIFromPlugin(const LilvPlugin* Plugin)
	{
		return lilv_node_as_string(lilv_plugin_get_uri(Plugin));
	}

	[[nodiscard]] static std::string GetCategoryFromPlugin(const LilvPlugin* Plugin)
	{
		return lilv_node_as_string(lilv_plugin_class_get_label(lilv_plugin_get_class(Plugin)));
	}


	[[nodiscard]] bool IsValid() const { return !URI.empty(); }
	[[nodiscard]] std::string GetURI() const { return URI; }
	[[nodiscard]] std::string GetName() const { return Name; }
	[[nodiscard]] std::string GetCategory() const { return Category; }

	[[nodiscard]] bool IsActive() const { return bIsActive && IsValid(); }
	void SetActive(bool bActive) { bIsActive = bActive; }

	[[nodiscard]] const std::vector<Port>& GetAudioInputPorts() const { return AudioInputPorts; }
	[[nodiscard]] const std::vector<Port>& GetAudioOutputPorts() const { return AudioOutputPorts; }
	std::vector<Port>& GetControlPorts() { return ControlPorts; }


private:
	bool bIsActive = true;


	std::string URI;
	std::string Name;
	std::string Category;


	std::vector<Port> AudioInputPorts;
	std::vector<Port> AudioOutputPorts;
	std::vector<Port> ControlPorts;
};
