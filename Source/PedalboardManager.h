#pragma once

#include "PedalBank.h"
#include "Pedalboard.h"

#include "vector"


enum class ESwapPedalboard : int8_t
{
	Previous = -1,
	Next = 1
};


class PedalboardManager
{
public:
	PedalboardManager();
	PedalboardManager(const PedalboardManager&) = delete;

	~PedalboardManager();


	void Initialize();


	void AddNewPedalboard();
	void RemoveCurrentPedalboard();
	void SwapCurrentPedalboardWith(ESwapPedalboard SwapWith);


	void MoveToPedalboard(ESwapPedalboard MoveTo);


	[[nodiscard]] Pedalboard& GetCurrentPedalboard() { return Pedalboards[CurrentPedalboardIndex]; }


	[[nodiscard]] uint32_t GetNextPedalboardIndex() const;
	[[nodiscard]] uint32_t GetPreviousPedalboardIndex() const;
	[[nodiscard]] uint32_t GetCurrentPedalboardIndex() const;


private:
	[[nodiscard]] static std::string GetConfigFilename(uint32_t Index);


	std::vector<Pedalboard> Pedalboards;

	uint32_t CurrentPedalboardIndex;
};
