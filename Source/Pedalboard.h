#pragma once

#include "ModHostClient.h"
#include "Pedal.h"
#include "PedalBank.h"

#include <vector>


enum class ESearchDirection : int8_t
{
	FromStart = 1,
	FromEnd = -1
};


enum class EPlugOperation : uint8_t
{
	Connect,
	Disconnect
};


enum class EPedalLocation : uint8_t
{
	Front,
	SFXLoop
};


class Pedalboard
{
public:
	explicit Pedalboard(uint32_t InIndex);
	Pedalboard() = delete;
	Pedalboard(const Pedalboard& Other) = delete;
	Pedalboard(Pedalboard&& Other) = default;


	Pedalboard& operator=(Pedalboard&& InPedalboard) = default;


	[[nodiscard]] bool IsActive() const { return bIsActive; }
	void Activate();
	void Deactivate();


	[[nodiscard]] bool LoadPedals() const;
	[[nodiscard]] bool UnloadPedals() const;


	void LoadPedalboardConfiguration();
	void SavePedalboardConfiguration() const;


	void SetNewIndex(uint32_t NewIndex, bool bRenameOrSave = true);


	[[nodiscard]] Pedal& GetPedalRefAt(uint32_t IndexSlot);


	[[maybe_unused]] bool AddPedalAt(Pedal&& InPedal, uint32_t IndexSlot, bool bLoad = true);
	void RemovePedalAt(uint32_t IndexSlot);
	void SwapPedalsAt(uint32_t IndexSlotA, uint32_t IndexSlotB);


	[[nodiscard]] bool TogglePedalAt(uint32_t IndexSlot);


private:
	[[nodiscard]] bool LoadPedal(uint32_t IndexSlot) const;

	[[nodiscard]] uint32_t GetBaseIndex() const { return NumPedals * Index; }


	[[nodiscard]] std::string GetConfigFilename() const;


	void PlugPedal(uint32_t IndexSlot, EPlugOperation PlugOperation, EPedalLocation PedalLocation) const;
	void PlugPedals(
		int32_t IndexSlotA, int32_t IndexSlotB, EPlugOperation PlugOperation, EPedalLocation PedalLocation) const;


	/** Where is this pedal located. Front or SFX Loop */
	[[nodiscard]] static inline EPedalLocation GetPedalLocation(uint32_t IndexSlot);

	/** -1 or NumPedals are special */
	[[nodiscard]] int32_t FirstValidPedalFrom(int32_t Reference, ESearchDirection SearchDirection) const;


	bool bIsActive;
	uint32_t Index;

	std::string Name;

	uint32_t NumPedals;
	std::vector<Pedal> Pedals;
};
