//
// Created by brunocooper on 5/6/23.
//

#include "Misc.h"

#include <array>
#include <memory>


std::string Misc::command_exec(const char* cmd)
{
	std::array<char, 128> buffer{};
	std::string result;
	std::unique_ptr<FILE, decltype(&pclose)> pipe(popen(cmd, "r"), pclose);
	if (!pipe)
	{
		return "";
	}

	while (fgets(buffer.data(), buffer.size(), pipe.get()) != nullptr)
	{
		result += buffer.data();
	}

	result.erase(result.end() - 1);

	return std::move(result);
}
