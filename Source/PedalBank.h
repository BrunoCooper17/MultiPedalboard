#pragma once

#include <memory>
#include <string>

/** TEMP STORAGE */
#include <map>
#include <vector>

#include "Pedal.h"


/**
 * PedalBank.
 * Hold all references of lv2 pedals found in the system.
 */
class PedalBank
{
public:
	PedalBank();
	PedalBank(const PedalBank& InPedalBank) = delete;
	PedalBank(PedalBank&& InPedalBank) noexcept;

	~PedalBank();


	[[nodiscard]] uint32_t LoadPedalBank();


	/** Returns a reference to all pedals loaded in categories */
	[[nodiscard]] const std::map<std::string, std::vector<Pedal>>& GetPedals() const { return PedalsLoaded; }

	/** Get reference of all categories of pedals that were loaded */
	[[nodiscard]] const std::vector<std::string>& GetCategories() const { return PedalCategories; }

	/** List of pedals of a category */
	[[nodiscard]] std::vector<Pedal> GetPedalsOfCategory(const std::string& Category) const;

	/** Get a copy of the pedal */
	[[nodiscard]] Pedal GetPedal(const std::string& Category, const std::string& UriName) const;

	/** How many pedals were loaded */
	[[nodiscard]] uint16_t GetAmountOfPedalsLoaded() const { return NumPedalsLoaded; }


	/** URIS */
	inline static LilvNode* AudioClass{ nullptr };
	inline static LilvNode* ControlClass{ nullptr };
	inline static LilvNode* InputClass{ nullptr };
	inline static LilvNode* OutputClass{ nullptr };


	/** Remove copy ability */
	PedalBank& operator=(const PedalBank& Other) = delete;
	PedalBank& operator=(PedalBank&& Other) = delete;


private:
	/** Pedal Map List */
	std::map<std::string, std::vector<Pedal>> PedalsLoaded;

	/** Cache: Categories to make easier to choose or search for a pedal */
	std::vector<std::string> PedalCategories;

	/** Cache: Amount of pedals loaded */
	uint16_t NumPedalsLoaded;


	/** LILV world */
	LilvWorld* World;


	/** Plugins Loaded with LILV */
	const LilvPlugins* Plugins;
};
