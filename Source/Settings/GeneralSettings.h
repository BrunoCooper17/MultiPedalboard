//
// Created by brunocooper on 4/8/23.
//

#pragma once

#include <cstdint>
#include <string>


struct GlobalSettingsData
{
	/** Max amount of pedalboards to keep track of */
	uint32_t MaxPedalboards = 20;
	/** Max amount of of pedals per pedalboard (Front) */
	uint32_t MaxPedals = 4;
	/** Max amount of of pedals per pedalboard (SFX) */
	uint32_t MaxPedalsSFX = 4;
	/** Default Pedalboard name */
	std::string DefaultName = "NoName";
	/** How many characters per line */
	uint32_t ScreenWidth = 20;
};

struct AudioSettingsData
{
	/** Are we going to have SFX Loop pedals? */
	bool bSFXLoop = true;
	/** Amount of In/Out Channels (mono, stereo). Set Channels to 1 if plan to use SFX Loop
	 (unless your audio card can provide more input/output channels) */
	uint32_t Front_InOutChannels = 1;
	/** Amount of Input/Out */
	uint32_t SFX_InOutChannels = 1;
	/** Input port name (without index) */
	std::string InputPortName = "capture_";
	/** Output port name (without index) */
	std::string OutputPortName = "playback_";
};

struct InputSettingsData
{
	/** Time to recognize as Hold action */
	double HoldTime = .75f;
	/** Time between taps to recognize as double tap */
	double DoubleTap = .25f;
};


class GeneralSettings
{
public:
	GeneralSettings();

private:
	/** [Pedalboards] */
	GlobalSettingsData GlobalSettings;

	/**	[Audio] */
	AudioSettingsData AudioSettings;

	/** [InputButtons] */
	InputSettingsData InputSettings;

public:
	[[nodiscard]] GlobalSettingsData const& GetGlobalSettings() const { return GlobalSettings; }
	[[nodiscard]] AudioSettingsData const& GetAudioSettings() const { return AudioSettings; }
	[[nodiscard]] InputSettingsData const& GetInputSettings() const { return InputSettings; }

private:
	[[nodiscard]] static std::string GetConfigFilename();
};
