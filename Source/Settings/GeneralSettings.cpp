//
// Created by brunocooper on 4/8/23.
//

#include "GeneralSettings.h"

#include "toml.hpp"

#include <filesystem>
#include <iostream>


GeneralSettings::GeneralSettings()
{
	try
	{
		const std::string SettingsFilename{ GeneralSettings::GetConfigFilename() };
		if (!std::filesystem::exists(SettingsFilename))
		{
			std::cerr << "Global Settings.toml file does not exists... using program defaults" << std::endl;
			return;
		}

		const auto& Settings{ toml::parse_file(SettingsFilename) };

		const auto& PedalboardData{ Settings["Pedalboards"] };
		GlobalSettings.MaxPedalboards = PedalboardData["MaxPedalboards"].as_integer()->get();
		GlobalSettings.MaxPedals = PedalboardData["MaxPedals"].as_integer()->get();
		GlobalSettings.MaxPedalsSFX = PedalboardData["MaxPedalsSFX"].as_integer()->get();
		GlobalSettings.DefaultName = PedalboardData["DefaultName"].as_string()->get();
		GlobalSettings.ScreenWidth = PedalboardData["ScreenWidth"].as_integer()->get();

		const auto& AudioData{ Settings["Audio"] };
		AudioSettings.bSFXLoop = AudioData["bSFXLoop"].as_boolean()->get();
		AudioSettings.Front_InOutChannels = AudioData["Front_InOutChannels"].as_integer()->get();
		AudioSettings.SFX_InOutChannels = AudioData["SFX_InOutChannels"].as_integer()->get();
		AudioSettings.InputPortName = AudioData["InputPortName"].as_string()->get();
		AudioSettings.OutputPortName = AudioData["OutputPortName"].as_string()->get();

		const auto& InputData{ Settings["InputButtons"] };
		InputSettings.HoldTime = InputData["HoldTime"].as_floating_point()->get();
		InputSettings.DoubleTap = InputData["DoubleTap"].as_floating_point()->get();
	}
	catch (const toml::parse_error& Exception)
	{
		std::cerr << "Error parsing file [" << Exception.source().path << "]: " << Exception.description() << " ("
				  << Exception.source().begin << ")" << std::endl;
	}
}


std::string GeneralSettings::GetConfigFilename()
{
	const std::string SettingsFile{ "/Multipedalboard/Settings.toml" };
	const std::string Path{ std::getenv("XDG_CONFIG_HOME") };
	return Path.empty() ? std::getenv("HOME") + std::string("/.config") + SettingsFile : Path + SettingsFile;
}
