#pragma once

#include "ModHostClient.h"
#include "PedalBank.h"

#include <iostream>


struct PedalboardInfo
{
	const PedalBank PedalBankRef;
	const ModHostClient ModHostClientRef;
	const GlobalSettingsData GlobalSettings;
	const Pedal Front_PedalPorts;
	const Pedal SFX_PedalPorts;

	const bool bSFXLoop;

	PedalboardInfo(PedalBank InPedalBank, ModHostClient InModHostClient, const GeneralSettings& InGeneralSettings)
		: PedalBankRef{ std::move(InPedalBank) }
		, ModHostClientRef{ std::move(InModHostClient) }
		, GlobalSettings{ InGeneralSettings.GetGlobalSettings() }
		, Front_PedalPorts{ InGeneralSettings.GetAudioSettings(), false }
		, SFX_PedalPorts{ InGeneralSettings.GetAudioSettings(), true }
		, bSFXLoop{ InGeneralSettings.GetAudioSettings().bSFXLoop }
	{
		std::cout << "Pedals Loaded: " << PedalBankRef.GetAmountOfPedalsLoaded() << std::endl;
	}
};

extern const PedalboardInfo g_info;
