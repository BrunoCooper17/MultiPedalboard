#include "PedalBank.h"
#include "Pedal.h"

/** LV2 */
#include "lv2/lv2plug.in/ns/ext/event/event.h"
#include "lv2/lv2plug.in/ns/ext/port-groups/port-groups.h"
#include "lv2/lv2plug.in/ns/ext/presets/presets.h"
#include "lv2/lv2plug.in/ns/lv2core/lv2.h"

/** Lilv */
#include "lilv-0/lilv/lilv.h"

#include <algorithm>
#include <iostream>
#include <utility>


PedalBank::PedalBank()
{
	std::cout << "PedalBank Constructor" << std::endl;

	World = lilv_world_new();
	lilv_world_load_all(World);

	AudioClass = lilv_new_uri(World, LILV_URI_AUDIO_PORT);
	ControlClass = lilv_new_uri(World, LILV_URI_CONTROL_PORT);
	InputClass = lilv_new_uri(World, LILV_URI_INPUT_PORT);
	OutputClass = lilv_new_uri(World, LILV_URI_OUTPUT_PORT);

	Plugins = lilv_world_get_all_plugins(World);

	NumPedalsLoaded = LoadPedalBank();
}


PedalBank::PedalBank(PedalBank&& InPedalBank) noexcept
	: PedalsLoaded{ std::move(InPedalBank.PedalsLoaded) }
	, PedalCategories{ std::move(InPedalBank.PedalCategories) }
	, NumPedalsLoaded(std::exchange(NumPedalsLoaded, 0))
	, World{ std::exchange(InPedalBank.World, nullptr) }
	, Plugins{ std::exchange(InPedalBank.Plugins, nullptr) }
{
}


PedalBank::~PedalBank()
{
	if (!World)
	{
		return;
	}

	uint32_t AmountOfPedalsLoaded{ 0 };
	for (const auto& [Category, PedalList] : PedalsLoaded)
	{
		AmountOfPedalsLoaded += PedalList.size();
	}

	std::cout << "PedalBank Destructor\nDestroying " << AmountOfPedalsLoaded << " pedals" << std::endl;

	lilv_node_free(PedalBank::AudioClass);
	PedalBank::AudioClass = nullptr;

	lilv_node_free(PedalBank::ControlClass);
	PedalBank::ControlClass = nullptr;

	lilv_node_free(PedalBank::InputClass);
	PedalBank::InputClass = nullptr;

	lilv_node_free(PedalBank::OutputClass);
	PedalBank::OutputClass = nullptr;

	lilv_world_free(World);
	World = nullptr;
}


uint32_t PedalBank::LoadPedalBank()
{
	if (!Plugins)
	{
		return 0;
	}

	LILV_FOREACH(plugins, Index, Plugins)
	{
		const LilvPlugin* PedalPlugin{ lilv_plugins_get(Plugins, Index) };
		const std::string Category{ Pedal::GetCategoryFromPlugin(PedalPlugin) };
		PedalsLoaded[Category].emplace_back(
			PedalPlugin, Pedal::GetURIFromPlugin(PedalPlugin), Pedal::GetNameFromPlugin(PedalPlugin), Category);
	}

	uint32_t AmountOfPedalsLoaded{ 0 };
	for (const auto& [PedalCategory, PedalsList] : PedalsLoaded)
	{
		PedalCategories.emplace_back(PedalCategory);
		AmountOfPedalsLoaded += PedalsList.size();
	}

	Plugins = nullptr;

	return AmountOfPedalsLoaded;
}


std::vector<Pedal> PedalBank::GetPedalsOfCategory(const std::string& Category) const
{
	if (const auto& Found{ PedalsLoaded.find(Category) }; Found != PedalsLoaded.cend())
	{
		return Found->second;
	}

	return {};
}


Pedal PedalBank::GetPedal(const std::string& Category, const std::string& UriName) const
{
	const std::vector<Pedal> Pedals{ GetPedalsOfCategory(Category) };

	const auto ByUriName{ [&UriName](const Pedal& Pedal)
		{
			return Pedal.GetURI() == UriName;
		} };

	const auto Found{ std::find_if(Pedals.cbegin(), Pedals.cend(), ByUriName) };
	if (Found != Pedals.cend())
	{
		return *Found.base();
	}

	return {};
}
