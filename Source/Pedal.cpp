#include "Pedal.h"
#include "Port.h"
#include "Settings/GeneralSettings.h"

#include <iostream>
#include <utility>


Pedal::Pedal(const LilvPlugin* Plugin, std::string InURI, std::string InName, std::string InCategory)
	: URI{ std::move(InURI) }
	, Name{ std::move(InName) }
	, Category{ std::move(InCategory) }
{
	// HOW MANY PORTS DOES IT HAVE AND WHAT ARE THE RANGE VALUES.
	const uint32_t NumberOfPorts{ lilv_plugin_get_num_ports(Plugin) };
	if (NumberOfPorts <= 0)
	{
		return;
	}

	float MinValues[NumberOfPorts];
	float MaxValues[NumberOfPorts];
	float DefaultValues[NumberOfPorts];
	lilv_plugin_get_port_ranges_float(Plugin, MinValues, MaxValues, DefaultValues);

	// CREATE EACH PORT
	for (uint32_t IndexPort{ 0 }; IndexPort < NumberOfPorts; ++IndexPort)
	{
		const LilvPort* Port{ lilv_plugin_get_port_by_index(Plugin, IndexPort) };
		const ETypePort PortType = Port::GetPortType(Plugin, Port);
		switch (PortType)
		{
		case ETypePort::AUDIO_IN:
			AudioInputPorts.emplace_back(Plugin, Port, PortType);
			break;

		case ETypePort::AUDIO_OUT:
			AudioOutputPorts.emplace_back(Plugin, Port, PortType);
			break;

		case ETypePort::CONTROL_IN:
			ControlPorts.emplace_back(
				Plugin, Port, PortType, MinValues[IndexPort], MaxValues[IndexPort], DefaultValues[IndexPort]);
			break;

		case ETypePort::INVALID:
			break;
		}
	}
}


/** This constructor is for System In/Out. So is an special invalid pedal */
Pedal::Pedal(const AudioSettingsData& AudioSettings, bool bSFXLoop)
{
	/**
	 * Depending of bool flags, we setup the ports of the "system" pedal. This pedal represents
	 * the device input/output
	 */
	const uint32_t StartPortIndex{ !bSFXLoop ? 0 : AudioSettings.Front_InOutChannels };
	const uint32_t EndPortIndex{ !bSFXLoop ? AudioSettings.Front_InOutChannels
										   : AudioSettings.Front_InOutChannels + AudioSettings.SFX_InOutChannels };
	for (uint32_t Index{ StartPortIndex }; Index < EndPortIndex; ++Index)
	{
		AudioInputPorts.emplace_back(AudioSettings.InputPortName + std::to_string(Index + 1), ETypePort::AUDIO_IN);
		AudioOutputPorts.emplace_back(AudioSettings.OutputPortName + std::to_string(Index + 1), ETypePort::AUDIO_OUT);
	}
}


void Pedal::PrintData() const
{
	std::cout << "Name: " << Name << "\tCategory: " << Category << "\t(" << URI << ")\n";
	if (!AudioInputPorts.empty())
	{
		std::cout << "Number Audio Inputs: " << AudioInputPorts.size() << "\n";
	}

	if (!AudioOutputPorts.empty())
	{
		std::cout << "Number Audio Outputs: " << AudioOutputPorts.size() << "\n";
	}

	if (ControlPorts.empty())
	{
		return;
	}

	std::cout << "ControlPorts: \n";
	for (const Port& ControlPort : ControlPorts)
	{
		ControlPort.PrintData();
	}
}

std::string Pedal::GetNameFromPlugin(const LilvPlugin* Plugin)
{
	LilvNode* NameNode{ lilv_plugin_get_name(Plugin) };
	std::string ReturnName{ lilv_node_as_string(NameNode) };
	lilv_node_free(NameNode);

	return ReturnName;
}
