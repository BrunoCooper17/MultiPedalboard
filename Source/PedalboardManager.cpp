#include "PedalboardManager.h"
#include "Settings/PedalboardInfo.h"

#include <filesystem>


PedalboardManager::PedalboardManager()
	: CurrentPedalboardIndex{ 0 }
{
}


PedalboardManager::~PedalboardManager()
{
	const uint32_t CurrentPedalboard{ GetCurrentPedalboardIndex() };
	if (!Pedalboards[CurrentPedalboard].UnloadPedals())
	{
		Pedalboards[CurrentPedalboard].Deactivate();
	}

	Pedalboards.clear();
}


void PedalboardManager::Initialize()
{
	uint32_t LastSaveIndex{ 0 };
	for (uint32_t CheckIndex{ 0 }; CheckIndex < g_info.GlobalSettings.MaxPedalboards; ++CheckIndex)
	{
		const std::string CheckConfigSave{ GetConfigFilename(CheckIndex) };
		if (!std::filesystem::exists(CheckConfigSave))
		{
			continue;
		}

		uint32_t PedalboardIndex{ CheckIndex };
		if (LastSaveIndex != PedalboardIndex)
		{
			std::filesystem::rename(CheckConfigSave, GetConfigFilename(LastSaveIndex));
			PedalboardIndex = LastSaveIndex;
		}

		Pedalboards.emplace_back(PedalboardIndex);
		++LastSaveIndex;
	}

	if (Pedalboards.empty())
	{
		/** Let's have a default pedalboard in case we don't have anything saved yet */
		Pedalboards.emplace_back(0);
	}

	Pedalboard& CurrentPedalboard{ GetCurrentPedalboard() };
	if (!CurrentPedalboard.LoadPedals())
	{
		CurrentPedalboard.Activate();
	}
}


uint32_t PedalboardManager::GetNextPedalboardIndex() const
{
	const uint32_t NextIndex{ CurrentPedalboardIndex + 1 };
	return NextIndex >= Pedalboards.size() ? 0 : NextIndex;
}


uint32_t PedalboardManager::GetPreviousPedalboardIndex() const
{
	const uint32_t PreviousIndex{ CurrentPedalboardIndex - 1 };
	return PreviousIndex >= Pedalboards.size() ? Pedalboards.size() - 1 : PreviousIndex;
}


uint32_t PedalboardManager::GetCurrentPedalboardIndex() const
{
	return CurrentPedalboardIndex;
}


void PedalboardManager::AddNewPedalboard()
{
	Pedalboard& CurrentPedalboard{ GetCurrentPedalboard() };
	if (!CurrentPedalboard.UnloadPedals())
	{
		CurrentPedalboard.Deactivate();
	}

	const uint32_t InsertIndex{ GetCurrentPedalboardIndex() };
	for (size_t idx{ Pedalboards.size() - 1 }; idx > InsertIndex; --idx)
	{
		/** Making sure the destination Save file does not exist */
		const std::string DestinationSaveFile{ GetConfigFilename(idx + 1) };
		if (std::filesystem::exists(DestinationSaveFile))
		{
			std::filesystem::remove(DestinationSaveFile);
		}

		Pedalboards[idx].SetNewIndex(idx + 1);
	}

	auto InsertedPedalboard = Pedalboards.emplace(Pedalboards.cbegin() + InsertIndex + 1, InsertIndex + 1);
	InsertedPedalboard->Activate();

	++CurrentPedalboardIndex;
}


void PedalboardManager::RemoveCurrentPedalboard()
{
	Pedalboard& PedalboardToRemove{ GetCurrentPedalboard() };
	if (!PedalboardToRemove.UnloadPedals())
	{
		PedalboardToRemove.Deactivate();
	}

	const uint32_t CurrentIndex{ GetCurrentPedalboardIndex() };
	std::filesystem::remove(GetConfigFilename(CurrentIndex));
	Pedalboards.erase(Pedalboards.cbegin() + CurrentIndex);

	const size_t LastIndex{ Pedalboards.size() };
	for (size_t idx{ CurrentIndex }; idx < LastIndex; ++idx)
	{
		Pedalboards[idx].SetNewIndex(idx);
	}

	--CurrentPedalboardIndex;
	CurrentPedalboardIndex = GetNextPedalboardIndex();

	if (Pedalboards.empty())
	{
		Pedalboards.emplace_back(0);
	}

	Pedalboard& CurrentPedalboard{ GetCurrentPedalboard() };
	if (!CurrentPedalboard.LoadPedals())
	{
		CurrentPedalboard.Activate();
	}
}


void PedalboardManager::SwapCurrentPedalboardWith(ESwapPedalboard SwapWith)
{
	const uint32_t SwapIndex{ SwapWith == ESwapPedalboard::Next ? GetNextPedalboardIndex()
																: GetPreviousPedalboardIndex() };
	const uint32_t CurrentIndex{ GetCurrentPedalboardIndex() };
	if (SwapIndex == CurrentIndex)
	{
		/** Nothing to do */
		return;
	}

	std::filesystem::remove(GetConfigFilename(CurrentIndex));
	std::filesystem::remove(GetConfigFilename(SwapIndex));

	Pedalboard& PreviousPedalboard{ GetCurrentPedalboard() };
	if (!PreviousPedalboard.UnloadPedals())
	{
		PreviousPedalboard.Deactivate();
	}

	std::iter_swap(Pedalboards.begin() + CurrentIndex, Pedalboards.begin() + SwapIndex);

	Pedalboards[SwapIndex].SetNewIndex(SwapIndex, false);
	Pedalboards[CurrentIndex].SetNewIndex(CurrentIndex, false);

	Pedalboards[SwapIndex].SavePedalboardConfiguration();
	Pedalboards[CurrentIndex].SavePedalboardConfiguration();

	CurrentPedalboardIndex = SwapIndex;
	Pedalboard& CurrentPedalboard{ GetCurrentPedalboard() };
	if (!CurrentPedalboard.LoadPedals())
	{
		CurrentPedalboard.Activate();
	}
}


void PedalboardManager::MoveToPedalboard(ESwapPedalboard MoveTo)
{
	/** Edge case... we only have one pedalboard */
	if (Pedalboards.size() < 2)
	{
		return;
	}

	CurrentPedalboardIndex = MoveTo == ESwapPedalboard::Next ? GetNextPedalboardIndex() : GetPreviousPedalboardIndex();
	const uint32_t NextIndex{ GetNextPedalboardIndex() };
	const uint32_t PreviousIndex{ GetPreviousPedalboardIndex() };

	const uint32_t PreviousPedalboardIndex = MoveTo == ESwapPedalboard::Next ? PreviousIndex : NextIndex;
	Pedalboard& PreviousPedalboard{ Pedalboards[PreviousPedalboardIndex] };
	if (!PreviousPedalboard.UnloadPedals())
	{
		PreviousPedalboard.Deactivate();
	}

	Pedalboard& CurrentPedalboard{ GetCurrentPedalboard() };
	if (!CurrentPedalboard.LoadPedals())
	{
		CurrentPedalboard.Activate();
	}
}


std::string PedalboardManager::GetConfigFilename(uint32_t Index)
{
	const std::string SettingsFile{ "/Multipedalboard/Pedalboard_" + std::to_string(Index) + ".toml" };
	const std::string Path{ std::getenv("XDG_CONFIG_HOME") };
	return Path.empty() ? Path + SettingsFile : std::getenv("HOME") + std::string("/.config") + SettingsFile;
}
