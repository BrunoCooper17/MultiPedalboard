#!/bin/bash

RED='\033[0;31m'
GREEN='\033[0;32m'
BLUE='\033[0;34m'
CYAN='\033[0;36m'
NC='\033[0m' # No Color

BUILD_CONFIG="Debug"
BUILD="true"
CLEAN="false"
REFRESH="false"

while [ "$1" != "" ]; do
  case $1 in
    --release)
      BUILD_CONFIG="Release"
      ;;
    --rebuild)
      CLEAN="true"
      REFRESH="true"
      ;;
    --clean)
      CLEAN="true"
      BUILD="false"
      ;;
    --refresh-project)
      REFRESH="true"
      BUILD="false"
      ;;
    esac
    shift
done

if [[ $CLEAN = "true" && -d "build" ]]; then
  echo -e "Cleaning up build directory ${RED}(rm -rf build)${NC}"
  rm -rf "build"
  if [[ -h "compile_commands.json" ]]; then
    rm "compile_commands.json"
  fi
fi

if [[ $BUILD = "true" ]]; then
  JOBS=$(grep -c ^processor /proc/cpuinfo)
  echo -e "${CYAN}Building project${NC} (Jobs: ${JOBS})"
  cmake -S . -B build -DCMAKE_BUILD_TYPE=$BUILD_CONFIG
  make --directory build --jobs $JOBS
fi

if [[ $REFRESH = "true" ]]; then
  echo -e "${GREEN}Updating ${BLUE}compile_commands.json${NC}..."
  cmake -S . -B build -DCMAKE_EXPORT_COMPILE_COMMANDS=ON && ln -sf build/compile_commands.json .
  echo -e "${GREEN}Done :)${NC}"
fi
